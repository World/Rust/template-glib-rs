// Generated by gir (https://github.com/gtk-rs/gir @ 6fbc68a47551)
// from 
// from gir-files (@ 7ebd4478b4a5)
// DO NOT EDIT

pub fn version() -> &'static str {
if cfg!(feature = "v3_36") {
        "3.36"
    } else {
        "1"
    }
}
