use crate::Expr;
use crate::Scope;
use glib::translate::*;
use std::ptr;

impl Expr {
    #[doc(alias = "tmpl_expr_eval")]
    pub fn eval(&self, scope: &Scope) -> Result<glib::Value, glib::Error> {
        unsafe {
            let mut return_value = glib::Value::uninitialized();
            let mut error = ptr::null_mut();
            let is_ok = ffi::tmpl_expr_eval(
                self.to_glib_none().0,
                scope.to_glib_none().0,
                return_value.to_glib_none_mut().0,
                &mut error,
            );
            assert_eq!(is_ok == glib::ffi::GFALSE, !error.is_null());
            if error.is_null() {
                Ok(return_value)
            } else {
                Err(from_glib_full(error))
            }
        }
    }
}
